from agora.abc import ParametersABC

from postprocessor.core.abc import PostProcessABC
from postprocessor.core.functions.tracks import get_joinable

# TODO Why not capitalized?
class mergerParameters(ParametersABC):
    """
    :param tol: float or int threshold of average (prediction error/std) necessary
        to consider two tracks the same. If float is fraction of first track,
        if int it is absolute units.
    :param window: int value of window used for savgol_filter
    :param degree: int value of polynomial degree passed to savgol_filter
    """

    _defaults = {
        "smooth": False,
        "tolerance": 0.2,
        "window": 5,
        "degree": 3,
        "min_avg_delta": 0.5,
    }


# TODO Why not capitalized?
class merger(PostProcessABC):
    """
    Combines rows of tracklet that are likely to be the same.
    """

    def __init__(self, parameters):
        super().__init__(parameters)

    def run(self, signal):
        joinable = []
        if signal.shape[1] > 4:
            joinable = get_joinable(signal, tol=self.parameters.tolerance)
        return joinable
