.. aliby documentation master file, created by
   sphinx-quickstart on Thu May 19 12:18:46 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:

   Home page <self>
   Installation <INSTALL.md>
   ..
      Examples <examples.rst>
   Reference <api.rst>
   ..
      Contributing <CONTRIBUTING.md>
   ..
      ALIBY reference <_autosummary/aliby>
      extraction reference <_autosummary/extraction>
      agora reference <_autosummary/agora>
      postprocessor reference <_autosummary/postprocessor>
      logfile_parser reference <_autosummary/logfile_parser>

.. include:: ../../README.md
   :parser: myst_parser.sphinx_
